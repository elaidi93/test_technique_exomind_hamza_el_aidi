//
//  CheckImageCache.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/20/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import UIKit

class CheckImageCashe {
    
    static let shared = CheckImageCashe()
    
    private let imageCache = NSCache<NSString, UIImage>()
    
    func downloadImage(strUrl: String, completion: @escaping (_ image: UIImage?, _ error: Error? ) -> Void) {
        let url = URL(string: strUrl)!
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            completion(cachedImage, nil)
        } else {
            
            MTAPIClient.downloadImage(url: url) { (data, error) in
                if let error = error {
                    completion(nil, error)
                    
                } else if let image = data {
                    self.imageCache.setObject(image, forKey: url.absoluteString as NSString)
                    completion(image, nil)
                } else {
                    completion(nil, NSError.generalParsingError(domain: url.absoluteString))
                }
            }
        }
    }
}
