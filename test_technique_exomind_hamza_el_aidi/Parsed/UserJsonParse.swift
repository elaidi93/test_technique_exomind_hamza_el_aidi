//
//  UserJsonParse.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/18/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import Foundation

class UserJasonParse {
    
    static let shared = UserJasonParse()
    
    func parsed(users json: [NSDictionary]) -> [User] {
        
        var users = [User]()
        
        for user in json {
            
            let id = user["id"] as! Int
            let name = user["name"] as! String
            let username = user["username"] as! String
            let mail = user["email"] as! String
            let tel = user["phone"] as! String
            let site = user["website"] as! String
            
            users.append(User(id: id, nom: name, pseudo: username, mail: mail, tel: tel, site: site))
            
        }
        
        return users
        
    }
    
}
