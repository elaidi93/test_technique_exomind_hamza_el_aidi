//
//  PhotoJsonParse.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/20/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import Foundation

class PhotoJsonParsed {
    
    static let shared = PhotoJsonParsed()
    
    func parsed(photos json: [NSDictionary]) -> [Photo] {
        
        var photos = [Photo]()
        
        for photo in json {
            
            let id = photo["id"] as! Int
            let album_id = photo["albumId"] as! Int
            let title = photo["title"] as! String
            let url = photo["url"] as! String
            let thumbnailUrl = photo["thumbnailUrl"] as! String
            
            photos.append(Photo(id: id, album_id: album_id, title: title, url: url, thumbnailUrl: thumbnailUrl))
            
        }
        
        return photos
        
    }
    
}
