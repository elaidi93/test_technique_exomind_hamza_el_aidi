//
//  AlbumJsonParsed.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/20/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import Foundation

class AlbumJsonParse {
    
    static let shared = AlbumJsonParse()
    
    func parsed(users json: [NSDictionary]) -> [Album] {
        
        var albums = [Album]()
        
        for album in json {
            
            let id = album["id"] as! Int
            let user_id = album["userId"] as! Int
            let title = album["title"] as! String
            
            albums.append(Album(id: id, user_id: user_id, title: title))
            
        }
        
        return albums
        
    }
    
}
