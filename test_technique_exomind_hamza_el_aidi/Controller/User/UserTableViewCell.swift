//
//  UserTableViewCell.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/18/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nom: UILabel!
    @IBOutlet weak var pseudo: UILabel!
    @IBOutlet weak var mail: UILabel!
    @IBOutlet weak var tel: UILabel!
    @IBOutlet weak var site: UILabel!
    
    @IBOutlet weak var container: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        container.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
