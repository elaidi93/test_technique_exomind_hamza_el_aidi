//
//  ViewController.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/18/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import UIKit

class UsersViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var unfiltredUsers = [User]()
    var users = [User]()
    var searchBar = UISearchController()

    override func viewDidLoad() {
        super.viewDidLoad()
        getUsers()
        registerCell()
        tableViewDelegate()
        addSearchBar()
    }
    
    func addSearchBar() {
        searchBar = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()

            tableView.tableHeaderView = controller.searchBar

            return controller
        })()
    }
    
    func tableViewDelegate() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func registerCell() {
        let nib = UINib(nibName: "UserCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "user_cell")
    }

    func getUsers() {
        UserWebService.shared.getUsers { response in
            self.unfiltredUsers = response as! [User]
            self.users = self.unfiltredUsers
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "album_segue" {
            let destination = segue.destination as! AlbumViewController
            destination.user_id = (sender as! Int)
        }
    }

}

extension UsersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "album_segue", sender: users[indexPath.row].getId())
    }
}

extension UsersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "user_cell", for: indexPath) as! UserTableViewCell
        cell.nom.text = users[indexPath.row].getnom()
        cell.pseudo.text = users[indexPath.row].getpseudo()
        cell.mail.text = users[indexPath.row].getmail()
        cell.tel.text = users[indexPath.row].gettel()
        cell.site.text = users[indexPath.row].getsite()
        
        return cell
    }
}

extension UsersViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        
        users.removeAll()
        let searchText = searchController.searchBar.text!
        
        if searchText == "" {
            users = unfiltredUsers
        } else {
            users = unfiltredUsers.filter {$0.getnom().lowercased().contains(searchText.lowercased()) || $0.getpseudo().lowercased().contains(searchText.lowercased()) || $0.getmail().lowercased().contains(searchText.lowercased()) || $0.gettel().lowercased().contains(searchText.lowercased()) || $0.getsite().lowercased().contains(searchText.lowercased())}
        }
        
        tableView.reloadData()
    }
}

