//
//  AlbumTableViewCell.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/20/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import UIKit

class AlbumTableViewCell: UITableViewCell {
    
    @IBOutlet weak var album_title: UILabel!
    @IBOutlet weak var container: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        container.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
