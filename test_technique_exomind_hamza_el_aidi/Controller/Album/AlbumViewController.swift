//
//  AlbumViewController.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/20/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import UIKit

class AlbumViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var albums = [Album]()
    var user_id: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getAlbums(user_id: user_id)
        registerCell()
        tableViewDelegate()
        
    }
    
    func tableViewDelegate() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func registerCell() {
        let nib = UINib(nibName: "AlbumCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "album_cell")
    }
    
    func getAlbums(user_id: Int) {
        AlbumWebService.shared.getAlbum(user_id: user_id) { response in
            self.albums = response as! [Album]
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "photo_segue" {
            let destination = segue.destination as! PhotosViewController
            destination.album_id = albums[sender as! Int].getId()
        }
    }

}

extension AlbumViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "photo_segue", sender: indexPath.row)
    }
}

extension AlbumViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "album_cell", for: indexPath) as! AlbumTableViewCell
        cell.album_title.text = albums[indexPath.row].getTitle()
        
        return cell
    }
}
