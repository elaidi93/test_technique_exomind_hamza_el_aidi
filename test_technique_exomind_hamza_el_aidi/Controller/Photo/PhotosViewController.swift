//
//  PhotosViewController.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/20/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import UIKit

class PhotosViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var album_id: Int!
    var photos = [Photo]()

    override func viewDidLoad() {
        super.viewDidLoad()

        getPhotos(user_id: album_id)
            registerCell()
            tableViewDelegate()
            
        }
        
        func tableViewDelegate() {
            tableView.delegate = self
            tableView.dataSource = self
        }
        
        func registerCell() {
            let nib = UINib(nibName: "PhotoCell", bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: "photo_cell")
        }
        
        func getPhotos(user_id: Int) {
            PhotoWebService.shared.getAlbum(album_id: album_id) { response in
                self.photos = response as! [Photo]
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        
        @IBAction func back(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }

}

extension PhotosViewController: UITableViewDelegate {}

extension PhotosViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "photo_cell", for: indexPath) as! PhotoTableViewCell
        
        cell.title.text = photos[indexPath.row].getTitle()
        CheckImageCashe.shared.downloadImage(strUrl: photos[indexPath.row].getThumbnailUrl()) { (image, error) in
            if error == nil {
                if let img = image {
                    DispatchQueue.main.async {
                        cell.img.image = img
                    }
                }
            }
        }
        
        return cell
    }
    
}
