//
//  UsersWebServices.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/18/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import Foundation
import Alamofire

class UserWebService {
    
    static let shared = UserWebService()
    
    func getUsers(_ callback: @escaping (Any)->()) {
        let users =  UserDataManager.shared.selectUsers()
        if users.count > 0 {
            callback(users)
        } else {
            let url = Constant.user_api_url
            AF.request(url).responseJSON { response in
                if response.response?.statusCode == 200 {
                    if let json = response.value as? [NSDictionary] {
                        let parsed = UserJasonParse.shared.parsed(users: json)
                        UserDataManager.shared.insertUsers(users: parsed)
                        callback(parsed)
                    }
                }
            }
        }
    }
    
}
