//
//  PhotoWebService.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/20/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import Foundation
import Alamofire

class PhotoWebService {
    
    static let shared = PhotoWebService()
    
    func getAlbum(album_id: Int, _ callback: @escaping (Any)->()) {
        let photos = PhotoDataManager.shared.selectPhotos(album_id: album_id)
        if photos.count > 0 {
            callback(photos)
        } else {
            let url = String(format: Constant.album_photo_api_url, "\(album_id)")
            AF.request(url).responseJSON { response in
                if response.response?.statusCode == 200 {
                    if let json = response.value as? [NSDictionary] {
                        let parsed = PhotoJsonParsed.shared.parsed(photos: json)
                        PhotoDataManager.shared.insertPhoto(photos: parsed)
                        callback(parsed)
                    }
                }
            }
        }
    }
}
