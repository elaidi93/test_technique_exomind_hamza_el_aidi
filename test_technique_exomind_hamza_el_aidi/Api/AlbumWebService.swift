//
//  AlbumWebService.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/20/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import Foundation
import Alamofire

class AlbumWebService {
    
    static let shared = AlbumWebService()
    
    func getAlbum(user_id: Int, _ callback: @escaping (Any)->()) {
        let albums = AlbumDataManager.shared.selectAlbums(user_id: user_id)
        if albums.count > 0 {
            callback(albums)
        } else {
            let url = String(format: Constant.user_album_api_url, "\(user_id)")
            AF.request(url).responseJSON { response in
                if response.response?.statusCode == 200 {
                    if let json = response.value as? [NSDictionary] {
                        let parsed = AlbumJsonParse.shared.parsed(users: json)
                        AlbumDataManager.shared.insertAlbum(albums: parsed)
                        callback(parsed)
                    }
                }
            }
        }
    }
}
