//
//  PhotoDataManager.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/20/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import UIKit
import CoreData

class PhotoDataManager {
    
    static let shared = PhotoDataManager()
    
    let photo_entity = "Photo_entity"
    
    func insertPhoto(photos: [Photo]) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        deletePhotos()
        let managedContext = appDelegate.persistentContainer.viewContext
        let photoEntity = NSEntityDescription.entity(forEntityName: photo_entity, in: managedContext)!
        
        for photo in photos {
            let weatherDetailEntity = NSManagedObject(entity: photoEntity, insertInto: managedContext)
            weatherDetailEntity.setValue(photo.getId(), forKey: "id")
            weatherDetailEntity.setValue(photo.getAlbumId(), forKey: "album_id")
            weatherDetailEntity.setValue(photo.getTitle(), forKey: "title")
            weatherDetailEntity.setValue(photo.getUrl(), forKey: "url")
            weatherDetailEntity.setValue(photo.getThumbnailUrl(), forKey: "thumbnailUrl")
        }
        
        do {
            try managedContext.save()
        } catch {
            print("save data error: \(error.localizedDescription)")
        }
        
    }
    
    func selectPhotos(album_id: Int) -> [Photo] {
        
        var photos = [Photo]()
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return photos }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: photo_entity)
        
        do {
            let predicate = NSPredicate(format: "album_id = %@", "\(album_id)")
            fetchRequest.predicate = predicate
            let result = try managedContext.fetch(fetchRequest)
            
            for data in result as! [NSManagedObject] {
                let photo = Photo(id: data.value(forKey: "id") as! Int, album_id: data.value(forKey: "album_id") as! Int, title: data.value(forKey: "title") as! String, url: data.value(forKey: "url") as! String, thumbnailUrl: data.value(forKey: "thumbnailUrl") as! String)
                photos.append(photo)
            }
        } catch {
            print("select Data error: \(error.localizedDescription)")
        }
        
        return photos
    }
    
    func deletePhotos() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: photo_entity)
        do {
            let items = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            
            for item in items {
                managedContext.delete(item)
            }
            
            do {
                try managedContext.save()
            } catch {
                print(error.localizedDescription)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
}
