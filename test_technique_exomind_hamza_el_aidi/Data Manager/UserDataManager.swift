//
//  UsersDataManager.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/19/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import UIKit
import CoreData

class UserDataManager {
    
    static let shared = UserDataManager()
    let user_entity = "User_entity"
    
    func insertUsers(users: [User]) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        deleteUsers()
        let managedContext = appDelegate.persistentContainer.viewContext
        let userEntity = NSEntityDescription.entity(forEntityName: user_entity, in: managedContext)!
        
        for user in users {
            let weatherDetailEntity = NSManagedObject(entity: userEntity, insertInto: managedContext)
            weatherDetailEntity.setValue(user.getId(), forKey: "id")
            weatherDetailEntity.setValue(user.getnom(), forKey: "nom")
            weatherDetailEntity.setValue(user.getpseudo(), forKey: "pseudo")
            weatherDetailEntity.setValue(user.getmail(), forKey: "mail")
            weatherDetailEntity.setValue(user.gettel(), forKey: "tel")
            weatherDetailEntity.setValue(user.getsite(), forKey: "site")
        }
        
        do {
            try managedContext.save()
        } catch {
            print("save data error: \(error.localizedDescription)")
        }
        
    }
    
    func selectUsers() -> [User] {
        
        var users = [User]()
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return users }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: user_entity)
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            for data in result as! [NSManagedObject] {
                let user = User(id: data.value(forKey: "id") as! Int, nom: data.value(forKey: "nom") as! String, pseudo: data.value(forKey: "pseudo") as! String, mail: data.value(forKey: "mail") as! String, tel: data.value(forKey: "tel") as! String, site: data.value(forKey: "site") as! String)
                users.append(user)
            }
        } catch {
            print("select Data error: \(error.localizedDescription)")
        }
        
        return users
    }
    
    func deleteUsers() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: user_entity)
        do {
            let items = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            
            for item in items {
                managedContext.delete(item)
            }
            
            do {
                try managedContext.save()
            } catch {
                print(error.localizedDescription)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
}
