//
//  AlbumDataManager.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/20/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import UIKit
import CoreData

class AlbumDataManager {
    
    static let shared = AlbumDataManager()
    
    let album_entity = "Album_entity"
    
    func insertAlbum(albums: [Album]) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        deleteAlbums()
        let managedContext = appDelegate.persistentContainer.viewContext
        let albumEntity = NSEntityDescription.entity(forEntityName: album_entity, in: managedContext)!
        
        for album in albums {
            let weatherDetailEntity = NSManagedObject(entity: albumEntity, insertInto: managedContext)
            weatherDetailEntity.setValue(album.getId(), forKey: "id")
            weatherDetailEntity.setValue(album.getUserId(), forKey: "user_id")
            weatherDetailEntity.setValue(album.getTitle(), forKey: "title")
        }
        
        do {
            try managedContext.save()
        } catch {
            print("save data error: \(error.localizedDescription)")
        }
        
    }
    
    func selectAlbums(user_id: Int) -> [Album] {
        
        var albums = [Album]()
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return albums }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: album_entity)
        
        do {
            let predicate = NSPredicate(format: "user_id = %@", "\(user_id)")
            fetchRequest.predicate = predicate
            let result = try managedContext.fetch(fetchRequest)
            
            for data in result as! [NSManagedObject] {
                let album = Album(id: data.value(forKey: "id") as! Int, user_id: data.value(forKey: "user_id") as! Int, title: data.value(forKey: "title") as! String)
                albums.append(album)
            }
        } catch {
            print("select Data error: \(error.localizedDescription)")
        }
        
        return albums
    }
    
    func deleteAlbums() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: album_entity)
        do {
            let items = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            
            for item in items {
                managedContext.delete(item)
            }
            
            do {
                try managedContext.save()
            } catch {
                print(error.localizedDescription)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
}
