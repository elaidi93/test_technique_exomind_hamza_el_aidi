//
//  Album.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/20/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import Foundation

class Album {
    
    private var id: Int!
    private let user_id: Int!
    private let title: String!
    
    init(id: Int, user_id: Int, title: String) {
        self.id = id
        self.user_id = user_id
        self.title = title
    }
    
    func getId() -> Int { return id }
    func getUserId() -> Int { return user_id }
    func getTitle() -> String { return title }
    
}
