//
//  Photo.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/20/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import Foundation

class Photo {
    
    private var id: Int!
    private let album_id: Int!
    private let title: String!
    private let url: String!
    private let thumbnailUrl: String!
    
    init(id: Int, album_id: Int, title: String, url: String, thumbnailUrl: String) {
        self.id = id
        self.album_id = album_id
        self.title = title
        self.url = url
        self.thumbnailUrl = thumbnailUrl
    }
    
    func getId() -> Int { return id }
    func getAlbumId() -> Int { return album_id }
    func getTitle() -> String { return title }
    func getUrl() -> String { return url }
    func getThumbnailUrl() -> String { return thumbnailUrl }
    
}
