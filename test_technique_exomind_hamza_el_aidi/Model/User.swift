//
//  User.swift
//  testtechniqueexomindhamzaelaidi
//
//  Created by hamza on 1/18/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import Foundation

class User {
    
    private var id: Int!
    private let nom: String!
    private let pseudo: String!
    private let mail: String!
    private let tel: String!
    private let site: String!
    
    init(id: Int, nom: String, pseudo: String, mail: String, tel: String, site: String) {
        self.id = id
        self.nom = nom
        self.pseudo = pseudo
        self.mail = mail
        self.tel = tel
        self.site = site
    }
    
    func getId() -> Int { return id }
    func getnom() -> String { return nom }
    func getpseudo() -> String { return pseudo }
    func getmail() -> String { return mail }
    func gettel() -> String { return tel }
    func getsite() -> String { return site }
    
}
