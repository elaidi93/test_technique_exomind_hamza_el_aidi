//
//  Constant.swift
//  test_technique_exomind_hamza_el_aidi
//
//  Created by hamza on 1/18/20.
//  Copyright © 2020 El Aidi. All rights reserved.
//

import Foundation

class Constant {
    
    static let user_api_url = "https://jsonplaceholder.typicode.com/users"
    static let user_album_api_url = "https://jsonplaceholder.typicode.com/users/1/albums?userId=%@"
    static let album_photo_api_url = "https://jsonplaceholder.typicode.com/users/1/photos?albumId=%@"
    
    
}
